const enterPrice = document.getElementsByClassName('input');
for (let i = 0; i < enterPrice.length; i++) {
    enterPrice[i].addEventListener("mouseover", function () {
        showTooltip(this, "input-tooltip");
    });
    enterPrice[i].addEventListener("mouseout", function () {
        hideTooltip(this, "input-tooltip");
    });

    function showTooltip(elem, className) {
        let span = document.createElement('span'); // создание  спана
        span.classList.add(className);
        span.textContent = elem.dataset.words;
        elem.appendChild(span);
    }

    function hideTooltip(elem, className) {
        const span = elem.getElementsByClassName(className)[0];
        span.remove()
    }

    input.onfocus = function () {

        if (this.className = 'error') {
            this.className = "";
            error.innerHTML = "";
        }
        else if (this.className = "focus") {
            this.className = "";
            error.innerHTML = "";
        }

    };

    input.onblur = function () {
        if (this.value <= 0) {
            this.className = "error";
            error.innerHTML = 'Please enter correct price'
        }
        else if (this.className = 'error') { // сбросить состояние "ошибка", если оно есть
            this.className = "";
            error.innerHTML = "";
        }
    }
}
